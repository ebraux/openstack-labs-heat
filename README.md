
# Labs Heat

La documentation générée est disponible sur le site [https://ebraux.gitlab.io/openstack-labs-heat/](https://ebraux.gitlab.io/openstack-labs-heat/)


## Tests en local du site


Build de l'image
```bash
docker build -t mkdocs_openstack-labs-heat .
```
rem : 
Creation du site
```bash
docker run  -v ${PWD}/..:/work mkdocs_openstack-labs-heat mkdocs new
```


Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_openstack-labs-heat mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_openstack-labs-heat mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_openstack-labs-heat rm -rf /work/site
docker image rm mkdocs_openstack-labs-heat
```


rem : Creation du site
```bash
docker run  -v ${PWD}/..:/work mkdocs_openstack-labs-heat mkdocs new
sudo chown -R $(id -u -n):$(id -g -n)  *
```


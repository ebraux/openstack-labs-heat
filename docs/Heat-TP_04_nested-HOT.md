# Création d'une Template Complexe


## Déroulement du TP

Ce TP a pour objectif d'aborder la problématique du déploiement de stacks composées de plusieurs éléments dépendants les uns des autres.

Le fil conducteur sera le déploiement de l'outil de blog `Wordpress`, dans une infrastructure composée de :

* Un réseau privé dédié.
* Une instance pour le serveur de base de données.
* Une instance pour le middletier web.

Plusieurs approches sont présentées :

* La création de template indépendantes : l'objectif est principalement de mettre au point chaque éléments : 
  * une template permettant de déployer un réseau privé de façon parametrable.
  * une template permettant de déployer une instance Mysql.
  * une template permettant de déployer un serveur web Apache/Php, et d'installer Wordpress.
* La création d'une template unique, rassemblent les différents élements : l'objetif est de montrer la difficulté de gérer ce type de fiLhier
* l'utilisation des "nested template" : l'objectif est de créer une template complexe à partir de template simples

La première étape consiste à valider l'installation de Wordpress sur une seule instance, attachée à un réseau existant.

Des template donnant la solution des execices sont disponibles à une adresse indiquée à la fin de chaque exercice. Elles peuvent être téléchargées en utilisant la commande `wget`:

```bash
wget URL -O solutions/FICHIER_DESTINATION
```

## Environnement du TP

Dans ce TP, le déploiement des templates est prévu avec le client en ligne de commande. Pour la mise en place de cet environnement, se reporter au TP "Utilisation du client Openstack en ligne de commande"

## Préparation de l'environnement de TP

* Vérification du fonctionnement du client openstack

```bash
openstack stack --help
heat --help
```

* création des dossiers de travail

```bash
mkdir -p ~/heat_cli_TP/hot
mkdir -p ~/heat_cli_TP/env
mkdir -p ~/heat_cli_TP/solutions
cd ~/heat_cli_TP
```

---

## Template "Instance Wordpress" ##

### Objectif ###

* Observer et tester une template de référence, permettant de déployer Wordpress dans une seule instance, connectée à un réseau existant.
* Cette template servira ensuite de base de travail pour les autres parties du TP.

Dans cette template :

* Les `parameters` sont :
  * 'key', 'name', 'image', 'flavor'
  * 'private_network', 'public_network', 'secgroup_name'
  * 'db_host_ip', 'db_name', 'db_user'
* dans les `resources`, les mots de passe **db_root_passwd** et **db_passwd** sont générés.
* les `outputs`sont : **instance_floating_ip** et **instance_password**

### Pré-requis ###

L'instance est connectée au réseau privé `private-network` qui :

* soit a déjà été créé lors du TP "Utilisation du client Openstack en ligne de commande"
* soit peut-être créé en déployant la template disponible à cette adresse : [private-network.yam](hot/private-network.yaml)

### Ressources ###

* Script de configuration des instances :

```bash
  #!/bin/bash

  # enable password Authentification, and set ubuntu password
  sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
  systemctl restart sshd
  echo "ubuntu:__user_password__" | chpasswd

  # Basic tools
  apt install -y htop vim
```

* Script d'installation de mysql :

```bash
  # mysql Installation
  export DEBIAN_FRONTEND=noninteractive
  apt install -q -y --force-yes mysql-server
  sed -i "s/\(bind-address.*=.* 127.0.0.1\).*/\#bind-address = 127.0.0.1/" \
    /etc/mysql/mysql.conf.d/mysqld.cnf
  systemctl restart mysql
  mysqladmin -u root password __db_root_password__

  # create a user and add remote privs to app
  mysql -u root --password="__db_root_password__" \
    -e "CREATE DATABASE __db_name__;"
  mysql -u root --password="__db_root_password__" \
    -e "CREATE USER '__db_user__'@'%' IDENTIFIED BY '__db_password__';"
  mysql -u root --password="__db_root_password__" \
    -e "GRANT ALL PRIVILEGES ON __db_name__.* to '__db_user__'@'%';"
  mysql -u root --password="__db_root_password__" \
    -e "FLUSH PRIVILEGES;"
```

* Script d'installation de Apache/Php/Wordpress :

```bash
  # apache/php Installation
  apt install -y apache2 php libapache2-mod-php php-mysql

  # install wordpress
  systemctl stop apache2
  rm -rf /var/www/html
  cd /var/www

  wget https://fr.wordpress.org/latest-fr_FR.tar.gz -O wordpress.tar.gz
  tar -xzf wordpress.tar.gz

  # configure wordpress
  cp wordpress/wp-config-sample.php wordpress/wp-config.php
  sed -i 's/database_name_here/__db_name__/' wordpress/wp-config.php
  sed -i 's/username_here/__db_user__/' wordpress/wp-config.php
  sed -i 's/password_here/__db_password__/' wordpress/wp-config.php
  sed -i 's/localhost/__db_host_ip__/' wordpress/wp-config.php

  mv wordpress html
  chown -R www-data:www-data /var/www/html
  systemctl start apache2
```

### Déploiement de la stack ###

* Récupération de la template wordpress [wordpress-instance.yaml](hot/wordpress-instance.yaml)


* Création du fichier env : `wordpress-instance-env.yaml` :

```yaml
parameters:
  key: heat-tp-cli
  name: wordpress-instance
  #image: imta-ubuntu-basic
  flavor: m1.medium
  private_network: private-network
  public_network: external
  secgroup_name: wordpress-instance
  #db_host_ip: localhost
  db_name: wordpress
  db_user: wordpress
```

* Déploiement de la template :

```bash
openstack stack create wordpress-instance \
 -t hot/wordpress-instance.yaml \
 -e env/wordpress-instance-env.yaml
```

* Relever les valeurs de l'Ip flottante de l'instance :

```bash
openstack stack output show \
  -c output_value -f value \
  wordpress-instance  instance_floating_ip
```

* Test : connection avec un navigateur à l'adresse IP_FLOTTANTE.

### Suppression de la stack ###

```bash
openstack stack delete wordpress-instance private-network
```

### Exemple de template Solution ###

* [wordpress-instance.yaml](hot/wordpress-instance.yaml)

---

## Template de déploiement du réseau privé dédié ##

### Objectif ###

* Ecrire une template permettant de créer une infrastructure réseau, de façon paramétrable.
* Basé sur le template : [private-network.yaml](hot/private-network.yaml)

* Nom du fichier : hot/generic-private-network.yaml.
* Parametres :
  * **network_name** : nom du réseau privé = **wp-network**.
  * **subnet_name** : nom du sous réseau attaché = **wp-subnet**.
  * **cidr** : plage d'adresse affactée = **172.16.10.0/24**.
  * **router_name** : nom du routeur a créer pour se connecter au réseau externe = **wp-router**.

### Ressources ###

* `parameters` :

```yaml
  network_name:
    type: string
    description: Name of the network
    default: private-network

  subnet_name:
    type: string
    description: Name of the subnet
    default: private-subnet

  cidr:
    type: string
    description: Sunet CIDR
    default: 172.16.1.0/24

  router_name:
    type: string
    description: Name of the router
    default: private-router

  public_network:
    type: string
    description: Name or Id of the public/external network
    default: external
```

### Lancement de la stack ###

* Création du fichier env : wordpress-network-env.yaml :

```yaml
parameters:
  network_name: wp-network
  subnet_name: wp-subnet
  cidr: 172.16.10.0/24
  router_name: wp-router
```

* Lancement :

```bash
openstack stack create wp-network \
  -t hot/generic-private-network.yaml \
  -e env/wordpress-network-env.yaml
```

* Vérification :

```bash
openstack stack show wp-network

openstack network list
openstack subnet list
openstack subnet  show wp-subnet
openstack router list
```

### Exemple de template Solution ###

* Fichier env : [wordpress-network-env.yaml](env/wordpress-network-env.yaml)
* Template : [generic-private-network.yaml](hot/generic-private-network.yaml)

---

## Template de déploiement de l'instance Mysql ##

### Objectif ###

* Ecrire une template permettant de déployer une instance Mysql de façon paramétrable.
* Basée sur la template : [wordpress-instance.yaml](hot/wordpress-instance.yaml)
* Nom du fichier : hot/mysql-instance.yaml
* Les `parameters` sont :
  * **mysql_instance_name**: nom de l'instance = **wp-mysql**
  * 'key', 'name', 'image', 'flavor'
  * 'private_network', 'public_network',
  * **mysql_admin_secgroup_name**: nom du groupe de sécurité pour les droits d'admin (ping et SSH), par défaut = 'mysql_admin_security_group'
  * **mysql_app_secgroup_name**: nom du groupe de sécurité pour les droits liés à mysql (port 3306), par défaut = 'mysql_app_security_group'
  * 'db_name' et 'db_user': nom de la base de données et de l'utilisateur associé = wordpress
* Les `outputs`sont :
  * 'instance_floating_ip' et 'instance_password'
  * **instance_private_ip**: l'IP de connection pour accéder à la base de données
  * **mysql_root_password**: le mot de passe d'admin de mysql
  * **app_db_password**: le mot de passe de la base de données.

#### Ressources ####

* `parameters` :

```yaml
  mysql_admin_secgroup_name:
    type: string
    label: Security Group Name
    description: Name of the security group to be used for Admin access
    default: mysql_admin_security_group

  mysql_app_secgroup_name:
    type: string
    label: Security Group Name for Mysql
    default: mysql_app_security_group
```

* `ressources` :

```yaml
  mysql_admin_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: { get_param: mysql_admin_secgroup_name }
      rules:
        - protocol: icmp
          remote_ip_prefix: 0.0.0.0/0
        - protocol: tcp
          port_range_min: 22
          port_range_max: 22
          remote_ip_prefix: 0.0.0.0/0

  mysql_app_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: { get_param: mysql_app_secgroup_name }
      rules:
        - protocol: tcp
          port_range_min: 3306
          port_range_max: 3306
          remote_ip_prefix: 0.0.0.0/0
```

* `outputs` : 

```yaml
  instance_private_ip:
    description: The Private IP assigned to the deployed instance
    value: { get_attr: [mysql_instance, first_address] }

  mysql_root_password:
    description: password for database root user
    value: { get_attr: [db_root_passwd,value] }

  app_db_password:
    description: database passwd
    value: { get_attr: [db_passwd,value] }
```

### Lancement de la stack ###

* Création du fichier env : wordpress-mysql-env.yaml.

```yaml
parameters:
  key: heat-tp-cli
  mysql_instance_name: wp-mysql
  #image: imta-ubuntu-basic
  #flavor: m1.small
  private_network: wp-network
  public_network: external
  #mysql_admin_secgroup_name: mysql_admin_security_group
  #mysql_app_secgroup_name: mysql_app_security_group
  db_name: wordpress
  db_user: wordpress
```

* Lancement :

```bash
openstack stack create wp-mysql \
  -t hot/mysql-instance.yaml \
  -e env/wordpress-mysql-env.yaml
```

* Vérification :

```bash
openstack stack show wp-mysql

ssh -i heat-tp-cli.pem ubuntu@__IP_FLOTTANTE__

> mysql -h __IP_FLOTTANTE__ -u wordpress -p wordpress
>  password : __APP_DB_PASSWORD__
> mysql> quit;
```

* Relever les valeurs de l'Ip interne de l'instance : 

```bash
openstack stack output show  \
  -c output_value -f value \
  wp-mysql instance_private_ip
```
* Relever les valeurs du database passwd :
```bash
`openstack stack output show \
  -c output_value -f value \
  wp-mysql app_db_password
```

### Exemple de template Solution ###

* Fichier env : [wordpress-mysql-env.yaml)](env/wordpress-mysql-env.yaml)
* Template : [mysql-instance.yaml](hot/mysql-instance.yaml)

---

## Template de déploiement de l'instance Apache/Php/Wordpress ##

### Objectif ###

* Ecrire une template permettant de déployer une instance Apache/Php/Wordpress de façon paramétrable.
* Basée sur la template : [wordpress-instance.yaml](hot/wordpress-instance.yaml)
* Nom du fichier : hot/apache-instance.yaml
* Les `parameters`sont :
  * **apache_instance_name**: nom de l'instance = `wp-apache`
  * 'key', 'name', 'image', 'flavor'
  * 'private_network', 'public_network',
  * **apache_admin_secgroup_name**: nom du groupe de sécurité pour les droits d'admin (ping et SSH), par défaut = 'apache_admin_security_group'
  * **apache_app_secgroup_name**: nom du groupe de sécurité pour les droits liés à apache (port 80), par défaut = 'appache_app_security_group'
  * **db_host_ip**, **db_name**, **db_user** et **db_password**: informations de connection à la base de données créée sur l'instance 'wp-mysql'
    * 'db_name' et 'db_user' doivent correspondre à ce qui a été indiqué pour la stack 'wp-mysql'
    * 'db_host_ip' et 'db_password' doivent correspondre aux valeurs `outputs`de la stack 'wp-mysql'
* les `outputs`sont :
  * 'instance_floating_ip' et 'instance_password'

### Ressources ###

* `parameters` :

```yaml
  apache_admin_secgroup_name:
    type: string
    label: Admin Security Group Name
    description: Name of the security group to be created for admin access
    default: apache_admin_security_group

  apache_app_secgroup_name:
    type: string
    label: Apache Security Group Name
    description: Name of the security group to be created for apache access
    default: apache_app_security_group

  db_host_ip:
    type: string
    label: Database host IP
    default: 127.0.0.1

  db_password:
    type: string
    label: Database user password
    default: app1
```

* `resources`

```yaml
  apache_admin_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: { get_param: apache_admin_secgroup_name }
      rules:
        - protocol: icmp
        - protocol: tcp
          port_range_min: 22
          port_range_max: 22

  apache_app_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: { get_param: apache_app_secgroup_name }
      rules:
        - protocol: tcp
          port_range_min: 80
          port_range_max: 80
```

### Lancement de la stack ###

* Création du fichier env : wordpress-apache-env.yaml

```yaml
parameters:
  key: heat-tp-cli
  apache_instance_name: wp-apache
  #image: imta-ubuntu-basic
  #flavor: m1.small
  private_network: wp-network
  public_network: external
  #apache_admin_secgroup_name: apache_admin_security_group
  #apache_app_secgroup_name: apache_app_security_group
  db_host_ip: __INSTANCE_PRIVATE_IP__
  db_name: wordpress
  db_user: wordpress
  db_password: __APP_DB_PASSWORD__
```

* Lancement :

```bash
openstack stack create wp-apache \
  -t hot/apache-instance.yaml \
  -e env/wordpress-apache-env.yaml
```

* Vérification :

```bash
openstack stack show wp-apache

ssh -i heat-tp-cli.pem ubuntu@IP_FLOTTANTE
```

* Relever la valeur de l'Ip flottante de l'instance :

```bash
openstack stack output show \
  -c output_value -f value \
  wp-apache  instance_floating_ip
```

* Test : connection avec un navigateur à l'adresse IP_FLOTTANTE

### Suppression des stacks ###

```bash
openstack stack delete  wp-apache wp-mysql wp-network
```

### Exemple de template Solution ###

* Fichier env : [env/wordpress-apache-env.yaml](env/wordpress-apache-env.yaml)
* Template : [hot/apache-instance.yaml](hot/apache-instance.yaml)

---

## Template de déploiement de Wordpress dans une infrastructure Ntier, avec réseau dédié ##

### Objectif ###

* Ecrire une template permettant de déployer wordpress dans un réseau dédié, avec un serveur de base de données séparé du serveur d'appliaction web.
* Basée sur la template : 
  * [generic-private-network.yaml](hot/generic-private-network.yaml)
  * [mysql-instance.yaml](hot/mysql-instance.yaml)
  * [apache-instance.yaml](hot/apache-instance.yaml)
* Nom du fichier : `hot/wordpress-full.yaml`

Cet exercice n'a pas grand interêt pedagogique, mis à part démontrer la complexité de manipuler ce type de fichier, et la quasi obligation d'utiliser les fonctionnalité de template "nested".

Il est donc possible d'utiliser directement la template solution.

### Récupération de la template et du fichier env ###

* Fichier env : [wordpress-ntier-env.yaml](env/wordpress-ntier-env.yaml)
* Template : [wordpress-ntier.yaml](hot/wordpress-ntier.yaml)

### Lancement de la stack ###

* Lancement :

```bash
openstack stack create wp-ntier \
  -t hot/wordpress-ntier.yaml \
  -e env/wordpress-ntier-env.yaml
```

* Vérification :

```bash
openstack stack show wp-ntier
```

* Relever la valeur de l'Ip flottante de l'instance :

```bash
openstack stack output show \
  -c output_value -f value \
  wp-ntier  instance_floating_ip
```

* Test : connection avec un navigateur à l'adresse IP_FLOTTANTE

### Suppression de la stack ###

```bash
openstack stack delete  wp-ntier
```

---

## Déploiement de l'application wordpress en mode "nested" dans une seule tempate ##

### Objectif ###

* Ecrire une template permettant de déployer wordpress dans un réseau dédié, avec un serveur de base de données séparé du serveur d'application web, en utilisant le principe des "Nested HOT"
* Basée sur la template : 
  * [generic-private-network.yaml](hot/generic-private-network.yaml)
  * [mysql-instance.yaml](hot/mysql-instance.yaml)
  * [apache-instance.yaml](hot/apache-instance.yaml)
* Nom du fichier : `hot/wordpress-nested.yaml`
* Les `parameters` sont :
  * Obligatoires :  **instance_floating_ip**
  * Autres :  mots de passe des instances, ....
* Les `outputs`sont :
  * Obligatoire :  **key**, **cidr**
  * Autres :  noms des différentes ressources, de la base de données pour wordpress, ...

### Ressources ###

* Appels des templates en mode "nested", non paramétrable :

```yaml
resources:

  network:
    type: private-network.yaml
    properties:
      network_name: wp-network
      subnet_name: wp-subnet
      cidr:  { get_param: cidr }
      router_name: wp-router

  mysql:
    type: mysql-instance.yaml
    properties:
      key: { get_param: key }
      mysql_instance_name: wp-mysql
      #image: imta-ubuntu-basic
      #flavor: m1.small
      private_network: wp-network
      public_network: external
      #mysql_admin_secgroup_name: mysql_admin_security_group
      #mysql_app_secgroup_name: mysql_app_security_group
      db_name: wordpress
      db_user: wordpress
    depends_on: network

  apache:
    type: apache-instance.yaml
    properties:
      key: { get_param: key }
      apache_instance_name: wp-apache
      #image: imta-ubuntu-basic
      #flavor: m1.small
      private_network: wp-network
      public_network: external
      #apache_admin_secgroup_name: apache_admin_security_group
      #apache_app_secgroup_name: apache_app_security_group
      db_host_ip: { get_attr: [mysql, instance_private_ip] }
      db_name: wordpress
      db_user: wordpress
      db_password:  { get_attr: [mysql, app_db_password] }
    depends_on: mysql
```

### Lancement de la stack ###

* exemple de fichier env : wordpress-nested-env.yaml :

```yaml
parameters:
  key: heat-tp-cli
  cidr: 172.16.10.0/24
```

* Lancement :

```bash
openstack stack create wp-nested \
  -t hot/wordpress-nested.yaml \
  -e env/wordpress-nested-env.yaml
```

* Vérification :

```bash
openstack stack show wp-nested

ssh -i heat-tp-cli.pem ubuntu@IP_FLOTTANTE
```

* Relever la valeur de l'Ip flottante de l'instance :

```bash
openstack stack output show \
  -c output_value -f value \
  wp-nested  instance_floating_ip
```

* Test : connection avec un navigateur à l'adresse IP_FLOTTANTE

### Suppression de la stack ###

```bash
openstack stack delete  wp-nested
```

### Exemple de template Solution ###

* Fichier env : [wordpress-nested-env.yaml](env/wordpress-nested-env.yaml)
* Template : [wordpress-nested.yaml](hot/wordpress-nested.yaml)

---

## Améliorations ##

### Utilisation du "resource_registry" pour référencer les template enfants ###

* Déclaration des types de ressources dans le fichier d'environnement :

```yaml
resource_registry:
  Lib::MSG::PrivateNetwork: https://ebraux.gitlab.io/openstack_heat_introdution/labs/hot/generic-private-network.yaml
  Lib::MSG::Mysql: https://ebraux.gitlab.io/openstack_heat_introdution/labs/hot/mysql-instance.yaml
  Lib::MSG::Apache: https://ebraux.gitlab.io/openstack_heat_introdution/labs/hot/apache-instance.yaml

parameters:
  key: heat-tp-cli
  cidr: 172.16.10.0/24

```

* Modification du fichier template pour utilser les types déclarés :

```yaml
resources:
  network:
    type: Lib::MSG::PrivateNetwork
    properties:
      ...

  mysql:
    type: Lib::MSG::Mysql
    properties:
      ...

  apache:
    type: Lib::MSG::Apache
    properties:
      ...
```

* Exemple de template Solution :
  * Fichier env : [env/wordpress-nested-registry-env.yaml](env/wordpress-nested-registry-env.yaml)
  * Template : [hot/wordpress-nested-registry.yaml](hot/wordpress-nested-registry.yaml)
  
### Création d'un repository de template ###

Créer un dossier 'lib', et y copier des templates de référence :

* réseau privé
* instance cirros
* instance ubuntu
* instance apache/php
* insatnce mysql
* instance wordpress
* instance midtier web wordpress
* Ip flottante
* ...

* Exemples : [lib](lib)

---

Auteur : emmanuel.braux@imt-atlantique.fr

Ce document est sous license Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR) selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

<http://creativecommons.fr/licences/>

![cc_by-nc-sa]( IMG/by-nc-sa.eu.png)

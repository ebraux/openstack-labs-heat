# Labs  Utilisation de Heat

Ces ateliers permettent tout d'abord de se familiariser avec Heat, et de mettre en place un environnement de travail de référence.

Ensuite, une approche progressive va permettre d'écrire des templates HOT de plus en plus complexes, et finalement de proposer un mode d'organisation efficient des templates.

Le déroulé des ateliers est le suivant :

1. [Utilisation de Heat avec Horizon](Heat-TP_01_horizon.md)
    * Interface HEAT dans Horizon
    * Déployer une stack
2. [Utilisation du client Openstack en ligne de commande](Heat-TP_02_clients.md)
    * Installation de l'environnement de travail
    * Deploiement d'une infrastructure réseau basique
    * Déploiement d'une instance simple
3. [Création de templates](Heat-TP_03_HOT-creation.md)
    * Template de lancement dune instance accessible en SSH
    * Personnalisation de l'instance au démarrage (user-data)
4. [Création d'une Template Complexe](Heat-TP_04_nested-HOT.md)
    * Déploiement d'une Instance Wordpress
    * Déploiement de Wordpress dans une infrastructure NTier
    * Déploiement en mode "nested"

Dans la première partie, une instance d' "administration" va être deployée avec Horizon. Elle sera sera ensuite utilisée dans la deuxième partie pour installer les clients openstack, puis pour déployer les stacks dans le reste des ateliers.

Dans la deuxième partie, un réseau privé va être déployé, qui permettra de déployer des instances et de pouvoir s'y connecter.

--- 

Pour récupérer l'intégratité du dépot et réaliser les labs 

```bash
git clone https://gitlab.com/ebraux/openstack-labs-heat.git
```

Pour telecharger un fichier de resources individuellement :
```bash
wget <URL du fichier>  -O <nom_local du fichier>
```

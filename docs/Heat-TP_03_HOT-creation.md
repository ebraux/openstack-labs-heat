# Création de templates


## Déroulement du TP

Ce TP a pour objectif l'écriture de templates HOT.

A chaque étape, les informations necessaires sont fournies pour ajouter des fonctionnalités à une template de référence.

Des templates donnant la solution des execices sont disponibles à une adresse indiquée à la fin de chaque exercice.

Pour récupérer l'intégratité du dépot et réaliser les labs 

```bash
git clone https://gitlab.com/ebraux/openstack_heat_introdution.git
```

Pour telecharger un fichier de resources individuellement en utilisant la commande `wget`:
```bash
wget <URL du fichier>  -O <nom_local du fichier>
```


## Environnement du TP

Dans ce TP, le déploiement des template est prévu avec le client en ligne de commande. Pour la mise en place de cet environnement, se reporter au TP "Utilisation du client Openstack en ligne de commande"

Il peut également être effectué avec Horizon. Dans ce cas, se reporter au TP "Utilisation de Heat avec Horizon"

Les instances sont connectées au réseau privé `private-network` qui :

* soit a déjà été créé lors du TP "Utilisation du client Openstack en ligne de commande"
* soit peut-être créé en déployant la template disponible à cette adresse : [hot/private-network.yaml](hot/private-network.yaml)

### Préparation de l'environnement de TP ###

* Vérification du fonctionnement du client openstack :

```bash
openstack stack --help
heat --help
```

* Création de l'arborescence de travail :

```bash
mkdir -p ~/heat_cli_TP/hot
mkdir -p ~/heat_cli_TP/env
mkdir -p ~/heat_cli_TP/solutions
cd ~/heat_cli_TP
```

---

## Template de référence ##

Disponible à l'url : [demo-instance.yaml](hot/demo-instance.yaml)

```yaml
heat_template_version: ocata

description: Demo simple instance

parameters:
  key:
    type: string
    label: Key name
    description: Name of key-pair to be used for compute instance
    default: my_key

resources:
  my_instance:
    type: OS::Nova::Server
    properties:
      image: cirros
      flavor: cirros
      key_name: { get_param: key }
      networks:
        - network: private-network

outputs:
  instance_name:
    description: The Name the deployed instance
    value: { get_attr: [my_instance, name] }

  instance_ip:
    description: The IP address of the deployed instance
    value: { get_attr: [my_instance, first_address] }
```

---

## Template de lancement dune instance accessible en SSH ##

### Objectifs ###

Améliorer la template basique créée précédement :

* Choisir le nom de l'instance.
* La rendre accessible en ssh : 
  * adresse IP flottante.
  * groupe de sécurité.
* Nom du fichier : hot/ssh-instance.yaml.
* En `parameters`:
  * **name** : nom de l'instance. Pas de valeur par defaut.
  * **key** : clé à utiliser pour l'authentification. Valeur par défaut **my_key'**.
* en `outputs`:
  * **instance_floating_ip** : l'adresse IP flottante de l'instance.

> Pour connecter l'instance au réseau privé, on utilisera une ressource 'port' attachée à une ressource réseau existante, puis on attachera cette ressource port à l'instance. Cette option est plus complexe, mais permet ensuite de manipuler des ressources 'security_group' et 'floating_ip' avec l'instance.

### Ressources ###

* `parameters`

```yaml
  name:
    type: string
    label: Instance Name
    description: Name of the compute instance
```

* Définition et utilisation d'un security_group :

```yaml
  my_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: ssh-instance
      rules:
        - protocol: icmp
        - protocol: tcp
          port_range_min: 22
          port_range_max: 22
```

* Définition d'un port associé à un security_group :

```yaml
  my_port:
    type: OS::Neutron::Port
    properties:
      network: private-network
      security_groups:
        - { get_resource: my_security_group }
```

* Ajout d'une propriété `name` dans une ressource server :

```yaml
    properties:
      ...
      name:  { get_param: name }
```

* Déclaration d'une propriété `networks` utilisant un port dans une ressources server :

```yaml
    properties:
      ...
      networks:
        - port: { get_resource: my_port }
```

* Définition d'une IP flottante et affectation à un port :

```yaml
  my_floating_ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: external
      port_id: { get_resource: my_port }
```

* Définition d'une Ip flottante en `outputs` :

```yaml
  instance_floating_ip:
    description: The Floating IP assigned to the deployed instance
    value: { get_attr: [my_floating_ip, floating_ip_address] }
```

### Lancement de la template ###

```bash
openstack stack create ssh-instance \
  -t hot/ssh-instance.yaml  \
  --parameter  "name=ssh-instance;key=heat-tp-cli"

openstack stack show ssh-instance

openstack server show ssh-instance

openstack stack output show ssh-instance --all

openstack stack output show  \
  -c output_value -f value \
  ssh-instance instance_floating_ip
```

### Vérification ###

* Test de connexion à l'instance

```bash
ssh -i heat-tp-cli.pem cirros@IP_FLOTTANTE
```

### Suppression de la stack ###

```bash
openstack stack delete ssh-instance
```

### Exemple de template Solution ###

* [ssh-instance.yaml)](hot/ssh-instance.yaml)

---

## Personnalisation de l'instance au démarrage (user-data) ##

### Objectifs ###

* Personnaliser une instance, en executant des commandes shell à l'initialisation.
* La template pécédente sert de base de travail.
* La modifcation du mot de passe de l'instance est ajoutée. 3 scénarios pour définir le mot de passe peuvent être testés :
  * une constante : fichier `passwd-constant-instance.yaml` (password = cirros)
  * un parametre saisi par l'utilisateur : fichier `passwd-parameters-instance.yaml`
  * une valeur générée de façon aléatoire : fichier `passwd-generated-instance.yaml`

### Ressources ###

####  Modification du mot de passe avec une constante  ####

* Ajout de la commande de changement de mot de passe sou forme de configuration "user_data" dans les propriétés de l'instance :

```yaml
  user_data_format: RAW
  user_data: |
    #!/bin/sh
    echo "cirros:secret" | chpasswd
```

####  Modification du mot de passe avec un parametre saisi par l'utilisateur  ####

* section `parameters` pour un mot de passe :

```yaml
  user_password:
    type: string
    label: Default user password
    description: Mot de passe de l'utilisateur par defaut
    default: secret
    hidden: true
```

* Modification de la configuration "user_data" pour utiliser un parametre :

```yaml
  user_data_format: RAW
  user_data:
    str_replace:
      params:
        __user_password__: { get_param: user_password }
          template: |
          #!/bin/sh
          echo "cirros:secret" | chpasswd
```

####  Modification du mot de passe avec une valeur générée aléatoirement  ####

* Génération automatique d'un mot de passe, section `ressources` :

```yaml
  my_instance_password:
    type: OS::Heat::RandomString
    properties:
      length: 16
      sequence: lettersdigits
```

* Modification de la configuration `user_data` pour utiliser une ressource et non un parametre :

```yaml
  __user_password__: { get_attr: [my_instance_password, value] }
```

* `outputs` pour afficher le mot de passe généré :

```yaml
  instance_password:
    description: password for the ubuntu user of the deployed instance
    value: {get_attr: [my_instance_password, value]}
```

### Test des différents scénarios ###

* Déploiement des templates :

```bash
openstack stack create passwd-instance \
  -t hot/passwd-xxxxxxx-instance.yaml \
  --parameter  "name=passwd-instance;key=heat-tp-cli"
```

* Récupération des informations pour se connecter : `openstack stack show passwd-instance`
* Test de connection : `ssh cirros@IP_FLOTTANTE`
* Suppression de la stack : `openstack stack delete passwd-instance`

> Dans le scénario "paramètre saisi par l'utilisateur", l'utilisation de la propriété `default` dans la définition du paramètre permet de ne pas être obligé d'indiquer de mot de passe lors du déploiement de la stack.

### Exemples de template Solution ###

* Une constante : [hot/passwd-constant-instance.yaml](hot/passwd-constant-instance.yaml)
* Un paramètre saisi par l'utilisateur : [hot/passwd-parameters-instance.yaml](hot/passwd-parameters-instance.yaml)
* Une valeur générée de façon aléatoire : [hot/passwd-generated-instance.yaml](hot/passwd-generated-instance.yaml)

---

## Template HOT générique de lancement d'instance ##

### Objectifs ###

* Ecrire une template permettant de lancer une instance Ubuntu, de façon paramétrable.
* nom du fichier : hot/ubuntu-instance.yml
* en `parameters`:
  * **name** : nom de l'instance. Pas de valeur par defaut.
  * **image** : nom de l'image. Valeur par défaut **imta-ubuntu-basic**.
  * **flavor** : nom du gabarit. Valeur par défaut **m1.small**
  * **key** : clé à utiliser pour l'authentification. Valeur par défaut **my_key**
  * **private_network** : réseau auquel attacher l'instance. Valeur par défaut **private-net**
  * **public_network** :  réseau sur lequel prendre une IP flottante. Valeur par défaut **external**
  * **secgroup_name** : nom du security groupe a créer pour le regles d'accès public. Valeur par défaut **public_access_security_group**

* en `outputs`:
  * **instance_floating_ip** : l'adresse IP flottante de l'instance
  * **instance_password** : mot de passe pour l'utilisateur Ubuntu

### Ressources ###

* `parameters` :

```yaml
  image:
    type: string
    label: Image name or ID
    description: Image to be used for compute instance
    default: imta-ubuntu-basic

flavor:
    type: string
    label: Flavor
    description: Type of flavor to be used
    default: m1.small

  private_network:
    type: string
    label: Private network name or ID
    description: Network to attach instance to.
    default: private-net

  public_network:
    type: string
    label: Public network name or ID
    description: Public network with floating IP addresses.
    default: external

  secgroup_name:
    type: string
    label: Security Group Name
    description: Name of the security group to be created for public access
    default: public_access_security_group
```

* Script "user-data" pour gestion du mot de passe avec Ubuntu :

```shell
  #!/bin/bash

  # enable password Authentification, and set ubuntu password
  sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
  systemctl restart sshd
  echo "ubuntu:__user_password__" | chpasswd
```

* Gestion des attributs pour les outputs :

Pour afficher les attributs disponibles pour une ressource : `value: { get_attr: [RESSOURCE, show] }`

### Lancement de la stack ###

```bash
openstack network list

openstack stack create ubuntu-instance \
  -t hot/ubuntu-instance.yaml \
  --parameter "name=ubuntu-instance" \
  --parameter "key=heat-tp-cli" \
  --parameter "private_network=private-network" \
  --parameter "secgroup_name=ubuntu-instance"

openstack stack show ubuntu-instance

openstack stack output show ubuntu-instance --all

openstack server show ubuntu-instance
```

### Test de connection ###

```bash
ssh -i heat-tp-cli.pem ubuntu@IP_FLOTTANTE`
```

### Suppression de la stack ###

```bash
openstack stack delete ubuntu-instance
```

### Exemple de template solution ###

* [ubuntu-instance.yaml](hot/ubuntu-instance.yaml)

---

## Tests de fonctionnalités complémentaires ##

### Objectif ###

* sur la base de la template "hot/ubuntu-instance.yml", tester différentes fonctionnalités :
  * ajout de contraintes sur les paramètres
  * utilisation d'un fichier pour les paramètres
  * ...

### Ajout de contraintes ###

* Exemple de contrainte :

```yaml
  constraints:
    - custom_constraint: glance.image
```

```yaml
  constraints:
    - `allowed_pattern: "[a-zA-Z0-9]*"`
```

Contraintes de format :

* caractères : `allowed_pattern: "[a-zA-Z0-9]*"`
* taille : `length: { min: 6, max: 8 }`

Contraintes par rapport à des ressources existantes :

* image : `custom_constraint: glance.image`
* flavor : `custom_constraint: nova.flavor`
* keypair : `custom_constraint: nova.keypair`
* network : `custom_constraint: neutron.network`

Exemple de template : [hot/ubuntu-constraint-instance.yml](hot/ubuntu-constraint-instance.yaml)

---

### Utilisation d'un fichier pour gérer les paramètres ###

* Utiliser l'option `-e`
* Fichier de parametres: `env/ubuntu-env.yaml`

```yaml
parameters:
  name: ubuntu-instance
  key: heat-tp-cli
  private_network: private-network
  secgroup_name: ubuntu-instance
```

* Utilisation

```bash
openstack stack create ubuntu-instance \
  -t hot/ubuntu-constraint-instance.yaml \
  -e env/ubuntu-env.yaml
```

### Exemple de template Solution ###

* Fichier env : [ubuntu-env.yaml)](env/ubuntu-env.yaml)
* Template : [ubuntu-constraint-instance.yaml](hot/ubuntu-constraint-instance.yaml)

---

Auteur : emmanuel.braux@imt-atlantique.fr

Ce document est sous license Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR) selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

<http://creativecommons.fr/licences/>

![cc_by-nc-sa]( IMG/by-nc-sa.eu.png)

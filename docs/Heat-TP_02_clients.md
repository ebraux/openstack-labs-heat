# Utilisation du client Openstack en ligne de commande

---

## Déroulement du TP

Ce TP propose une prise en main de Heat, utilisé avec le client openstack en ligne de commande.

La première étape consiste à installer l'environnement de travail sur une instance vierge, déployée par exemple avec horizon.

Les étapes suivantes permettent de déployer :

* Un réseau privé, relié au réseau externe permettant d'obtenir des IP flottantes.
* Une instance dans ce réseau, avec une IP flottante associée, et un groupe de sécurité permettant de se connecter en SSH.

Cette instance peut ensuite être utilisée pour déployer d'autres stacks.

Les templates utilisées peuvent paraître complexes, mais l'objectif de ce TP n'est pas d'étudier le contenu des templates, mais de maitriser leur déploiement avec le Horizon. La création de templates est abordée dans d'autres TP.

---

## Environnement du TP

Pour réaliser ce TP il est nécessaire de disposer :

* D'un projet dans Openstack
* D'un poste de travail avec :
  * Un outil permettant de se connecter en ssh  à un serveur distant (ssh en shell, putty, ...).
  * Un outil permettant de copier des fichier sur un serveur distant  (scp en shell, filezilla, ...).
  * Un acces réseau vers la classe d'IP flottantes en SSH pour accèder aux instances créés.

L'installation et l'utilsation des clients Openstack est prévue depuis une instance elle même déployée dans Openstack :

* Soit lors du TP "Utilisation de Heat avec Horizon"
* Soit en déployant la template disponible à cette adresse : [hot/os-admin.yaml](hot/os-admin.yaml)

Il est toutefois possible de réaliser ces TP depuis un poste local. Dans ce cas, le poste local doit disposer :

* Des droits necesaires pour pouvoir ajouter un repository de package, et installer des packages.
* D'outils pour se connecter à distance et copier des fichier sur un serveur (ssh et scp).
* Des accès ouvert vers l'infrastructure openstack :
  * Port 5000, pour interragir avec l'infrastructure.
  * Un acces réseau vers la classe d'IP flottantes en SSH pour accèder aux instances créées.

---

## Installation de l'environnement de travail

### Objectifs

* Installer un environnement de travail pour pouvoir utiliser le client openstack et manipuler / gérer des stacks.
* mettre en place la configuration de l'authentification
* créer une paire de clé dans Openstack pour le TP

> Rem : l'installation des clients Openstack est réalisée à partir des dépots Openstack, mais il est également possible de les installer avec "**python-pip**" et l'outil `virtualenv` qui permet de mettre en place un environnement python indépendant de celui de la machine hôte (voir annexes : "Installation des clients Openstack et Heat avec python-pip").

### Installation du client openstack et du client heat

Connection à la machine distante 'os-cli' pour installer les clients

> Ignorer cette étape si vous utilisez votre poste de travail local.


Connexion

```bash
ssh -i heat-tp-horizon.pem ubuntu@IP_FLOTANTE
```

Configuration du repository "rocky" (version du serveur)

```bash
sudo apt install software-properties-common
sudo add-apt-repository -y cloud-archive:rocky
sudo apt update && sudo apt -y dist-upgrade
```

Installation des "clients" Openstack

```bash
sudo apt install -y python-openstackclient python-heatclient
```

Vérification

```bash
openstack --help
openstack stack --help
```

### Préparation de l'environnement de TP ###

```bash
mkdir -p ~/heat_cli_TP/hot
cd ~/heat_cli_TP
```

### configuration de l'authentification ###

L'authentification pas fichier `os-cloud` va être utilisée :

* création du fichier "clouds.yaml" et modification de vos informations de connection : [clouds.yaml](clouds.yaml)

```yaml
clouds:
  mycloud:
    region_name: 'RegionOne'
    domain_name: 'Default'
    auth:
      username: '__VOTRE_LOGIN__'
      password: '__VOTRE_MOT_DE_PASSE__'
      project_name: '__VOTRE_PROJET__'
      auth_url: 'https://openstack.imt-atlantique.fr:5000/v3'
    identity_api_version: 3
    verify: false
```
* Test : `openstack --os-cloud=mycloud flavor list`
* Mise en place d'un alias pour le confort : `alias openstack='openstack --os-cloud=mycloud'`
* Test avec l'alias : `openstack flavor list`

### Génération d'une paire de clé pour le TP ###

Une paire de clé spécifique à ce TP est créée.

Il est toutefois possible d'utiliser une clé déjà existante, mais dans ce cas, vous devez copier le fichier '.pem' sur la machine depuis laquelle vous réalisez ce TP.

* Génération de la paire de cles :

```bash
openstack keypair create heat-tp-cli > heat-tp-cli.pem
chmod 600 heat-tp-cli.pem
cat heat-tp-cli.pem
```

* Récupération de la clé publique :

```bash
openstack keypair show --public-key heat-tp-cli > heat-tp-cli.pub
chmod 600 heat-tp-cli.pub
cat heat-tp-cli.pub
```

---

## Deploiement d'une infrastructure réseau basique ##

### Objectifs ###

* observer un fichier HOT complet
* lancer une stack avec le client openstack
* observer le résultat

### Topologie de l'infractucture réseau ###

![Topologie du réseau]( IMG/private-network.png)




### Lancement de la template ###

* Fichier template : [hot/private-network.yaml](hot/private-network.yaml)

* Lancement

```bash
openstack stack create \
  private-network \
  -t hot/private-network.yaml
```

* Vérification

```bash
openstack stack show private-network

openstack stack resource list private-network

openstack stack resource show \
  private-network private_router

openstack stack resource show \
  -c resource_name \
  -c physical_resource_id \
  -c resource_status \
  private-network private_router

openstack stack resource metadata  \
  private-network private_subnet

openstack stack event list \
  private-network

openstack stack event show \
  private-network private_network EVENT
```

### Vérification des différents éléments créés

```bash
openstack network show private-network
openstack subnet show private-subnet
openstack router show private-router
```

---

## Déploiement d'une instance simple 

### Objectifs

* Déployer une instance de test "cirros", connectée au réseau 'private-network' créé précédement.
* Collecter des informations avant de créer une stack.
* Utiliser des parametres.
* Utiliser des informations en sortie.

### Collecte des informations sur son environnement

On instancie une `image`, en lui affectant des ressources par l'intermédiaire d'un `gabarit`. On y accède en `réseau`, et on s'y connecte en s'authentifiant à l'aide d'une `clé`.

Les commandes ci-dessous permettent de collecter les informations nécéssaires :

* Liste des clés disponibles `openstack  keypair list`
* Liste des gabarits `openstack flavor list`
* Liste des images `openstack image list`
* Liste des réseaux `openstack network list`


### Déploiement de la template

* Fichier template : [hot/demo-instance.yaml](hot/demo-instance.yaml)

* Lancement de la template

```bash
openstack stack create demo-instance -t hot/demo-instance.yaml --parameter "key=heat-tp-cli"

openstack stack show demo-instance
```

* Affichage des informations en sortie :

```bash
openstack stack output show demo-instance --all

openstack stack output show -c output_value -f value demo-instance  instance_ip
```

> Le nom de l'instance a été calculé par Heat

> Il n'est pas possible de se connecter à l'instance car :
>
> * Elle n'a pas d'IP flottante.
> * Elle n'est pas dans le même sous réseau que la machine `os-cli`.
> * Aucun groupe de sécurité ne lui a été affecté.

* Suppression de la stack

```bash
openstack stack delete demo-instance
```

---

## Annexes ##

### Installation des clients Openstack et Heat avec python-pip ###

#### Installation de Virtualenv ####

* Installation

```bash
# Install de virtualenv
sudo apt install virtualenv virtualenvwrapper

# Prerequis pour les installations via setuptools,
# pkg_resources, pip, wheel...
apt install -y  build-essential autoconf libtool python-dev
```

>`virtualenv` et `virtualenvwrapper` modifient la configuration des fichier `profile` par défaut du système.
> Il faut donc ouvrir une nouvelle session après l'installation pour qu'ils soient fonctionnels.

> Rappels sur l'utilisation de virtualenv :
>
>	* Lister les environnement : `workon`
>	* Créer un environnement : `mkvirtualenv my_env`
> * Selectionner un environnement : `workon my_env`
> * Sortir d'un environnement : `deactivate`

#### Installation de l'environnement client Openstack ####

* Installation

```bash
mkvirtualenv heat_cli_TP
workon heat_cli_TP
pip install python-openstackclient==3.16.*
pip install python-heatclient==1.16.*
```

* Vérification

```bash
openstack stack --help
heat --help
```

---

Auteur : emmanuel.braux@imt-atlantique.fr

Ce document est sous license Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR) selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

<http://creativecommons.fr/licences/>

![cc_by-nc-sa]( IMG/by-nc-sa.eu-small.png)

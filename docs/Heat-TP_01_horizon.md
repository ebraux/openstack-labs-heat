# Utilisation de Heat avec Horizon

---

## Déroulement du TP ##

Ce TP propose une prise en main de Heat, utilisé avec le tableau de bord Horizon.

Il consiste à déployer une instance à partir d'un fichier template, et de s'y connecter. La stack proposée permet de déployer :

* un réseau privé, relié au réseau externe permettant d'obtenir des IP flottantes
* une instance dans ce réseau, avec une IP flottante associée.
* un groupe de sécurité permattant de se connecter en SSH

Cette instance peut ensuite être utilisée pour installer des outils complémentaires. C'est, par exemple, à partir de cette instance que sont réalisés les TPs utilisant le client Openstack en ligne de commande (d'où le nom `os-cli`).

Les templates utilisées peuvent paraître complexes, mais l'objectif de ce TP n'est pas d'étudier le contenu des templates, mais de comprendre le principe de déploiement des template. La création de templates est abordée dans d'autres TP.

---

## Environnement du TP ##

Pour réaliser ce TP il est necessaire de disposer :

* d'un projet dans Openstack
* d'un poste de travail avec :
  * un navigateur.
  * un outil permettant de se connecter en ssh  à un serveur distant : ssh en shell, mobaXTerm ([https://mobaxterm.mobatek.net/(https://mobaxterm.mobatek.net/)]), ....
  * un accès réseau vers Openstack en HTTPS pour accèder au tableau de bord horizon.
  * un acces réseau vers la classe d'IP flottantes en SSH pour accèder à l'instance créée.

Il est également necessaire d'avoir créé une paire de clés dans Openstack, et de disposer de la clé privée sur son poste de travail.

Si ce n'est pas le cas, il est indispensable d'en créer une :

1. Se connecter au tableau de bord Horizon
2. Dans le menu de navigation à gauche, ouvrir le menu `Projet` / `Compute` et choisir  la catégory `Paires de clés`
3. Cliquer sur le bouton `Créer une paire de clé` (en haut à droite)
4. Dans la fenêtre qui s'affiche, saisir le nom de la clé : `heat-tp-horizon` (par exemple)
5. Cliquer sur `Créer une paire de clé` et sauvegarder le fichier `heat-tp-horizon.pem` sur votre poste de travail, dans votre environnement utilisateur.

> Attention, le fichier 'heat-tp-horizon.pem' est unique
> Il ne pourra pas être re-généré.
> Vous devez le conserver dans un endroit sûr, et vous rappeler de cet endroit.

---

## Découverte de l'Interface HEAT dans Horizon ##

**Objectif**

* Parcourir les différents menus dédié à Heat disponibles dans Horizon.

**Connection à Horizon**

1. Se connecter au tableau de bord Horizon.
2. Dans le menu de navigation à gauche, ouvrir le menu `Projet` / `Orchestration`.

Explorer  les différentes catégories :

* **Stacks** : les stacks en cours
* **Types de ressources** : les types de `resources` disponibles sur cette infrastructure :
  * Les attributs.
  * Les propriétés.
* **Version du modèle** : correspond à  `heat_template_version` dans un fichier HOT :
  * Les différentes versions du modèle.
  * La description des fonctions que chaque version supporte.

---

## Déployer une stack avec Horizon

### Objectifs

* Lancer une stack avec Horizon.
* Comprendre les parametres demandés par défaut

### Contexte

La stack proposée permet de déployer une instance dans un evironnemnt réseau dédié (réseau, groupes de securité, ...).


### Accès à Heat

1. Se connecter au tableau de bord Horizon
2. Sélectionner le projet "HeatTP_xx" dans le menu déroulant en haut à gauche
3. Dans le menu de navigation sur le côté gauche, ouvrir le menu `Projet` / `Orchestration` et choisir  la catégory `Stacks`
4. Cliquer sur le bouton `Lancer la Stack` (en haut à droite)

### Etape 1 : Saisie du modèle

Le modèle est le fichier template (HOT). Sa source peut être :

* **Fichier** : un fichier stocké en local sur votre station de travail
* **Entrée Directe** : du texte saisi directement dans l'interface
* **Url** : une url vers le fichier de template

Le champ `environnement` est optionnel, et correspond à la fonction avancée de heat permettant de personnaliser des ressources. Sa source peut-être "Fichier" ou "Entrée Directe".

Dans notre cas, nous allons utiliser un fichier template disponible **depuis une URL, sans utiliser d'environnement** : 

* url : [os-admin.yaml](hot/os-admin.yaml)
  
Une fois l'url saisie, cliquer sur **suivant**, ce qui déclenche :

* La vérification de la syntaxe de la template
* L'affichage d'un écran de saisie des parametres définis dans la template.

### Etape 2 : Saisie des parametres

**Nom de la stack**

Un nom "parlant".
Dans le cas présent, la stack créée des ressources réseau préfixées par `os-cli`, et l'instance `os-cli`.

On peut donc choisir le nom `os-cli`.

**Retour en arrière en cas d'echec**

Heat propose 2 modes de gestion des ressources en cas d'échec :

* soit la suppression de toutes les ressources créées : dans ce cas il ne reste aucune information sur la raison de l'échec du déploiement.
* soit ne rien supprimer : il est alors possible d'inspecter la stack, pour comprendre la raison de l'échec du déploiement. Mais il faut ensuite supprimer la stack manuellement.

Dans notre cas, le mode choisi est de *"ne rien supprimer en cas d'échec"*.

**Délai d'attente à la création**

Si la stack n'a pas été déployée au bout de ce délai, le déploiement est considéré en échec.

Attention, celà inclu la section `user-data` des instances. Une stack peut être en état 'COMPLETE', mais si des actions `user-data` dépassent le délai, les instances sont stopées, et la stack passe en état 'ECHEC'.

Laisser la *valeur par défaut : 60mn*

**Mot de passe de l'utilisateur**

Pour que le Heat  puisse faire évoluer la Stack sans intervention de l'utilisateur (cas de HA, ou d'autoscaling), il a besoin des droits sur le projet et les ressources. Dans le mode par défaut, Heat stocke le mot de passe crypté dans sa base de données.

Cette option peut être ignorée si on pas besoin des fonctionnalités HA et autoscaling.

De plus, ce mode de fonctionnement, a été remplacé par un nouveau mécanisme de `trust` ne nécéssitant plus la conservation du mot de passe par Heat. Le mot de passe de l'utilistateur n'est donc plus nécéssaire.C'est ce mécanisme qui est en place sur l'infrastructure Openstack.

Mais dans Horizon, c'est un champ obligatoire.

En conclusion, il faut saisir quelque chose, mais cette information n'est pas utilisée ensuite.

**Autres parametres**

Dans la stack, le paramètre "key" a été défini, correspondant à une `keypair` existante.

Selectionner la clé `heat-tp-horizon` (ou votre clé) dans la liste. Vous aurez besoin de la clé privée pour vous connecter à l'instance `os-admin` par la suite.

### Etape 3 : lancement de la stack

Cliquer ensuite sur **"Lancer la stack"**.
Votre stack apparait dans la liste des stacks.

Cliquer sur votre stack, puis sur l'onglet **"Topologie"** pour suivre en direct la création des différents éléments.

Consulter les autres onglets :

* **"Ressources"** pour voir les ressources créés.
* **"Evénements"** pour voir les différentes opérations réalisées par heat.
* **"Modèle"** pour consulter le fichier template utilisé pour déployer la stack.

Enfin, cliquer sur l'onglet **"Vue d'ensemble"** pour consulter :

* les informations sur la stack et son état.
* les différentes informations de déploiement (projet, keypair, ...).
* mais surtout les `outputs` et dans le cas présent ce qui nous interesse : l'**adresse IP flottante** de l'instance "os-cli", ainsi que mle **mot de passe** pour une connection sans clé.

> Dans les `outputs` le mot de passe apparait rapidement, mais pour l'Ip flottante il faut attendre qu'elle soit active.

### Vérification du déploiement ###

**Visualisation des ressources déployées**

Dans le menu de navigation à gauche, ouvrir le menu `Projet` / `Réseau` et choisir  la catégorie `Topologie du réseau`

![Topologie du réseau](IMG/heat-tp_os-cli.png)

Il est également possible de consulter la liste des routeurs, réseaux, groupes de sécurité, ip flottantes et instances

---

## Connection à l'instance ##

### Connection depuis un poste de travail Linux ###

Une fois que vous avez l'ip flottante, vous pouvez vous connecter à l'instance.

```bash
ssh -i heat-tp-horizon.pem ubuntu@IP_FLOTANTE
```

ou

```bash
ssh ubuntu@IP_FLOTANTE
  password>  PASSWORD
```


### Avertissement ###

La phase d'installation de paquets et de mise à jour peut prendre quelques minutes.
Une fois cette phase terminée, votre instance va redémarrer automatiquement.
Pour vérifier que l'installation est terminée, dans le message de login, vous devez avoir :

```bash
 0 packages can be updated.
 0 updates are security updates.
```

---

## Informations sur les autres opérations disponibles sur une stack ##

Dans la fenêtre de gestion des stacks, on retrouve quelques actions possibles sur les stacks :

* **Prévisualiser la stack** (Stack Preview) :
  * simule un déploiement sans créer de ressources.
* **Vérifier la pile** (Check stack) : vérifie que toutes les ressources sont dans l'état souhaité.
* **Interrompre la pile** (Suspend stack) : interrompt l'execution de la stack.
  * suspend les instances.
  * n'affecte pas les réseaux.
  * ne libère pas de quotas.
* **Reprendre la pile** (Resume Stack) : Reprend l'execution de la stack.
* **Changer le modèle de stack** (Change Stack Template) : permet de mettre à jour la template.
  * correspond à un `stack update`.
* **supprimer la stack** (Delete stack) : supprime la stack.

---



Auteur : emmanuel.braux@imt-atlantique.fr

Ce document est sous license Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR) selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

<http://creativecommons.fr/licences/>

![cc_by-nc-sa]( IMG/by-nc-sa.eu-small.png)
